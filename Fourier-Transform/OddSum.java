package PracticeTest;

public class OddSum 
{
    public static void main(String[] args) 
    {
        int total = 0;
        for(int i = 0; i <=200; i++)
        {
            if(i % 5 == 0 && i % 2 != 0)
            {
                total +=i;
            }
        }
        System.out.println("The sum of odd numbers divisible by 5 from 0 to 200 "
                            + "is " + total);
    }
}
