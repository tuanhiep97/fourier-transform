package PracticeTest;

import java.util.Scanner;

public class CandyGame {

    public static void main(String[] args) {
        boolean game = true;
        boolean choose = false;
        boolean turn = true;
        boolean player = true;
        boolean eaten = false;
        int eat = 0;
        int candy = 23;
        String select = "";
        Scanner sc = new Scanner(System.in);
        while(game)
        {
            System.out.println("Welcome to the Candy game!");
            System.out.print("Enter your name: ");
            String name = sc.nextLine();
            System.out.println("Hello, " + name);
            while(!choose)
            {
                System.out.print("Select head or tail (H/T): ");
                select = sc.nextLine();
                if(!"H".equals(select) && !"h".equals(select) && !"t".equals(select) && !"T".equals(select))
                {
                    System.out.println("You can only choose H or T. Try again");
                }else{
                    choose = true;
                }
            }
            double random = Math.random();
            String coin = "";
            if (random < 0.5) {
                coin = "H";
            } else {
                coin = "T";
            }
            if (select.toUpperCase().equals(coin)) {
                if (select.toUpperCase().equals("H")) {
                    System.out.println("The result is Head, you go first");
                } else if (select.toUpperCase().equals("T")) {
                    System.out.println("The result is Tail, you go first");
                }
            } else {
                if (select.toUpperCase().equals("H")) {
                    System.out.println("The result is Head, I go first");

                } else if (select.toUpperCase().equals("T")) {
                    System.out.println("The result is Tail, I go first");
                }
                turn = false;
            }           
            while(candy > 0)
            {
                if(turn == true)
                {
                    while(!eaten)
                    {
                        System.out.print("Your turn: ");
                        eat = sc.nextInt();
                        if(eat > 2 || eat <= 0)
                        {
                            System.out.println("You can only eat 1 or 2 candy. Choose again");
                        }else{
                            sc.nextLine();
                            eaten = true;
                        }
                    }
                    candy -= eat;
                    System.out.println("You eat " + eat + " candy, there are " + candy + " candy remain");
                    player = true;
                    if(candy == 0){
                        break;
                    }
                    double comp = Math.random();
                    int compEat = 0;
                    if(comp < 0.5)
                    {
                        compEat = 1;
                    }else{
                        compEat = 2;
                    }
                    candy -= compEat;
                    System.out.println("I eat " + compEat + " candy, there are " + candy + " candy remain");
                    player = false;
                    eaten = false;
                    if(candy == 0){
                        break;
                    }
                }else
                {
                    double comp = Math.random();
                    int compEat = 0;
                    if(comp < 0.5)
                    {
                        compEat = 1;
                    }else{
                        compEat = 2;
                    }
                    candy -= compEat;
                    System.out.println("I eat " + compEat + " candy, there are " + candy + " candy remain");
                    player = false;
                    eaten = false;
                    if(candy == 0){
                        break;
                    }
                    while(!eaten)
                    {
                        System.out.print("Your turn: ");
                        eat = sc.nextInt();
                        if(eat > 2 || eat <= 0)
                        {
                            System.out.println("You can only eat 1 or 2 candy. Choose again");
                        }else{
                            sc.nextLine();
                            eaten = true;
                        }
                    }
                    candy -= eat;
                    System.out.println("You eat " + eat + " candy, there are " + candy + " candy remain");
                    player = true;
                    if(candy == 0){
                        break;
                    }
                }
            }
            if(player == true)
            {
                System.out.println("YOU WIN!!!");
            }else{
                System.out.println("COMPUTER WIN!!!");
            }
            System.out.print("Do you want to play again(y/n): ");
            String cont = sc.nextLine();
            if(cont.toUpperCase().equals("Y"))
            {
                game = true;
            }else{
                game = false;
                System.out.println("Thanks for playing.");
            }
        }
    }
}
